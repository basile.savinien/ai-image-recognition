# zoidberg2.0_2020_3

# Modele CNN keras / Tensorflow + bonus bacterien/viral

## Comment le lancer ?

Afin de lancer et tester le modele Keras / Tensorflow, il faudra se rendre dans le sous-dossier "CNN Tensorflow".

Il vous faudra telecharger toutes les dépendance de ce projet ci. Pour cela un fichier de dépendance requirements.txt a été créé. Il vous faudra faire dans une interface de commande:

```python
pip install -r requirements.txt
```

Une fois dedans, vous serez presenté par deux fichier .ipynb, il faudra lancer le modele "model_setup_final.ipynb".

Attention, il faut que le set de donnée chest X_ray se situe au meme endroit que le modele afin de ne pas avoir d'erreur de path lorsque le code va chercher les images. Je conseille de copier coller les deux fichiers sur votre pc au niveau de votre set de données.

Il suffit ensuite de faire tout executer (un snap de code par un snap de code, ou tout d'un coup). Le temps total d'exécution dependra fortement de votre CPU (ou GPU si set-up correctement), surtout pour la construction du modele. Il y a la construction de deux modeles:

- un pour le sujet classique, c'est à dire, la différenciation entre une pneumonie et une image normale
- un pour le bonus, permettant de determiner le type de pneumonie, c'est à dire Virale ou Bactérienne

Enfin, vous pourrez utiliser le fichier result.ipynb ou vous pourrez choisir une image via chemin d'access et faire tourner le modele de prediction dessus. Voici un exemple pour moi :

```python
fn = "person149_bacteria_713.jpeg"
fn2 = "person78_virus_140.jpeg"
fn3 = "IM-0089-0001.jpeg"

path = 'chest_Xray/test//PNEUMONIA/' + fn2
```

Le result s'affiche en dessous avec le bonus en plus de dire si la pneumonie est bacterienne ou virale.

![image](https://user-images.githubusercontent.com/73825379/117284960-b8c95980-ae67-11eb-9e07-929a28b6f2bc.png)


# Modele Random Tree

Se rendre dans le sous dossierr: 'Random Forest Tree Classifier'

Pour lancer le modèle Random Forest Classifier, vous devez utilisez le dataset chestXray.
Vous devez placer le dossier chest_Xray dans le dossier Random Forest Tree Classifier.
Il faudra ensuite installer les dépendances du projet et pour cela, il suffit de faire la commande 
```python
pip install -r requirements.txt
```

Il suffit ensuite de lancer le jupyter notebook.

# Modele KNN

Se rendre dans le sous dossierr: 'KNN SkLearn'

Pour lancer le modèle Knn, vous devez utilisez le dataset chestXray. Vous devez placer le dossier chest_Xray dans le dossier KNN SkLearn, installer les dépendances du projet avec la commande

pip install -r requirements.txt

puis lancer le jupyter notebook.



# Modele bonus : 

## VGG16 PyTorch

Se rendre dans le sous dossierr: 'Bonus_models' -> puis le dossier 'PyTorch'

Pour lancer le projet il vous faut d'abord un set de données chestXray (https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia)

Après avoir téléchargé les images de test il vous faut installer les dépendances pour ce projet. Un fichier requirements.txt est disponible.
Il vous suffit de lancer la commande

```python
pip install -r requirements.txt
```

Après avoir vérifié dans le code que les paths vers les dossier de data sont correct (lignes 47,48 et 49) vous êtes prêts à lancer le programme

Vous pouvez maintenant lancer le notebook jupyter (pytorch.ipynb)
